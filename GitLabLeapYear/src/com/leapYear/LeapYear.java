package com.leapYear;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		int year;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter any year : ");
		year = sc.nextInt();
		sc.close();
		boolean isleap = false;
		if(year%4==0) {
			if(year%100 ==0) {
				if(year % 400 ==0) {
					isleap = true;
				}
			}else {
				isleap = true;
			}
		}
		if(isleap) {
			System.out.println(" Year : "+year+" is leap year !");
		}else {
			System.out.println(" Year : "+year+" is not a leap year ");
		}
	}
}
